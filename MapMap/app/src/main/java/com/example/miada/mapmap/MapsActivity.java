package com.example.miada.mapmap;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private static final LatLng JOHN_ABBOTT_COLLEGE = new LatLng(45.405430, -73.941996);
    private static final LatLng OLD_PORT = new LatLng(45.512031, -73.546454);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        //SHOW COLLEGE WITH ICON AND ZOOMED
//        mMap.addMarker( new MarkerOptions()
//                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.johnabbott))
//                .anchor(0.0f, 1.0f)
//                .title("John Abbott College")
//                .position(JOHN_ABBOTT_COLLEGE));
//
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(JOHN_ABBOTT_COLLEGE)
//                .zoom(16)
//                .bearing(0)
//                .tilt(30)
//                .build();
//
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        goToLocationZoom(45.512031, -73.546454, 15);


        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    //TO GO TO SPECIFIC LOCATION ZOOMED ON IT
    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mMap.moveCamera(update);
    }

    public void geoLocate(View view) throws IOException {
//        EditText etWhere = findViewById(R.id.etWhere);
//        String location = etWhere.getText().toString();
//
//        Geocoder geocoder = new Geocoder(this);
//        List<Address> list = geocoder.getFromLocationName(location, 1);
//        Address address = list.get(0);
//        String locality = address.getLocality();
//
//        Toast.makeText(this, locality, Toast.LENGTH_LONG).show();
//        double lat = address.getLatitude();
//        double lng = address.getLongitude();
//        goToLocationZoom(lat, lng, 15);
//        setMarker(locality, lat, lng);

    }
}
