package com.johnabbottcollege.androidteamproject;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class LocationListAdapter extends ArrayAdapter<Parking> {
    private Activity context;
    List<Locations>locationsList;

    public LocationListAdapter(Activity context, List<Locations> parkingList) {
        super(context, R.layout.location_item);
        this.context = context;
        this.locationsList = parkingList;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.location_item,null,true);

        TextView tvdate = (TextView)listViewItem.findViewById(R.id.tv_date);

        TextView tvAddress = listViewItem.findViewById(R.id.tv_address);

        Locations locations = locationsList.get(position);
        tvdate.setText(String.valueOf(locations.getLng()));

        return listViewItem;


    }
}
