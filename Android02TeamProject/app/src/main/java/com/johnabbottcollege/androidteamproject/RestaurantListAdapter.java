package com.johnabbottcollege.androidteamproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class RestaurantListAdapter extends ArrayAdapter<Restaurant> {
    private Activity context;
    List<Restaurant>restaurantList;

    public RestaurantListAdapter(Activity context, List<Restaurant> restaurantList) {
        super(context, R.layout.location_item,restaurantList);
        this.context = context;
        this.restaurantList = restaurantList;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.location_item,null,true);

        TextView tvdate = (TextView)listViewItem.findViewById(R.id.tv_date);

        TextView tvAddress = listViewItem.findViewById(R.id.tv_address);

        Restaurant Restaurant = restaurantList.get(position);
        tvdate.setText(Restaurant.getCurrentDate());
        tvAddress.setText(Restaurant.getAddress());

        return listViewItem;


    }
}
