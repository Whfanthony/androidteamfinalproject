package com.johnabbottcollege.androidteamproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ShoppingListAdapter extends ArrayAdapter<Shopping> {
    private Activity context;
    List<Shopping>shoppingList;

    public ShoppingListAdapter(Activity context, List<Shopping> shoppingList) {
        super(context, R.layout.location_item,shoppingList);
        this.context = context;
        this.shoppingList = shoppingList;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.location_item,null,true);

        TextView tvdate = (TextView)listViewItem.findViewById(R.id.tv_date);

        TextView tvAddress = listViewItem.findViewById(R.id.tv_address);

        Shopping shopping = shoppingList.get(position);
        tvdate.setText(shopping.getCurrentDate());
        tvAddress.setText(shopping.getAddress());

        return listViewItem;


    }
}
