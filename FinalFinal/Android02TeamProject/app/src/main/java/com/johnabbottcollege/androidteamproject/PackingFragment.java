package com.johnabbottcollege.androidteamproject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.johnabbottcollege.androidteamproject.MainActivity.TAG;


public class PackingFragment extends Fragment {
    public static final String TAG = "Packing Fragment";
    //Test
//        ListView myListView;
//        String[] strListView;
    DatabaseReference databaseParking;
    //private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

    ListView listViewParking;
    //ParkingListAdapter<String> parkingListAdapter;
   ArrayAdapter<String>arrayAdapter;
    ArrayList<String> parkingList;

    View view;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.layout_parking_fragmen, container, false);
        //Test
//        myListView = (ListView) view.findViewById(R.id.lv_parking_location);
//        strListView = getResources().getStringArray(R.array.my_data_list);
//        ArrayAdapter<String>objAdapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,strListView);
//        myListView.setAdapter(objAdapter);


        databaseParking = FirebaseDatabase.getInstance().getReference().child("address_parking");

        listViewParking = (ListView) view.findViewById(R.id.lv_parking_location);
        parkingList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String> (getContext(),android.R.layout.simple_list_item_1,parkingList);// new ParkingListAdapter(getActivity(), parkingList);
        listViewParking.setAdapter(arrayAdapter);

        fetchData();
        return view;
    }

    private void fetchData() {
        databaseParking.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

//                for (DataSnapshot parkingSnapshot : dataSnapshot.getChildren()) {
//                    Parking parking = parkingSnapshot.getValue(Parking.class);
//                    parkingList.add(parking);
//                }

                Iterator iterator = dataSnapshot.getChildren().iterator();
                Set <String> set = new HashSet<>();
                while (iterator.hasNext()) {
                    set.add(((DataSnapshot) iterator.next()).getKey());
                }
                parkingList.clear();
                parkingList.addAll(set);

                arrayAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onStart() {
        super.onStart();

        databaseParking.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
